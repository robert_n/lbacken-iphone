//
//  Item.h
//  BeerCrate
//
//  Created by Robert Nordström on 2013-03-24.
//  Copyright (c) 2013 Robert N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (nonatomic, retain) NSString *id;
@property (nonatomic, retain) NSString *itemId;
@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *name2;
@property (nonatomic, retain) NSString *alcohol;
@property (nonatomic, retain) NSString *volume;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *pricePerLiter;
@property (nonatomic, retain) NSString *year;
@property (nonatomic, retain) NSString *yearTaste;
@property (nonatomic, retain) NSString *category;
@property (nonatomic, retain) NSString *assortment;
@property (nonatomic, retain) NSString *origin;
@property (nonatomic, retain) NSString *originCountry;
@property (nonatomic, retain) NSString *producer;
@property (nonatomic, retain) NSString *supplier;
@property (nonatomic, retain) NSString *ecological;
@property (nonatomic, retain) NSString *koscher;
@property (nonatomic, retain) NSString *packaging;
@property (nonatomic, retain) NSString *module;
@property (nonatomic, retain) NSString *startAt;
@property (nonatomic, retain) NSString *endAt;

@end
