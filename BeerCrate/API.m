//
//  API.m
//  BeerCrate
//
//  Created by Robert Nordström on 2013-03-24.
//  Copyright (c) 2013 Robert N. All rights reserved.
//

#import "API.h"

@implementation API

+ (API *)sharedInstance
{
    static API *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

+ (NSArray *)parseJSON:(NSDictionary *)json intoObjectOfType:(Class)objectClass
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (NSDictionary *row in json) {
        
        id item = [[objectClass alloc] init];
        
        for (NSString *key in [row allKeys]) {
            NSString *name = [key stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[key substringToIndex:1] lowercaseString]];
            [item setValue:[row objectForKey:key] forKey:name];
        }
        
        [result addObject:item];
    }
    
    return result;
}

- (void)request:(JSONResponseBlock)completion
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:[@"http://beercrate.azurewebsites.net/assortments?$filter=substringof('Öl', Category) eq true&$top=50" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        completion(json);
    }];
}

@end
