//
//  BeerDetailViewController.m
//  BeerCrate
//
//  Created by Robert Nordström on 2013-03-23.
//  Copyright (c) 2013 Robert N. All rights reserved.
//

#import "BeerDetailViewController.h"

@interface BeerDetailViewController ()

@end

@implementation BeerDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.item.name;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
