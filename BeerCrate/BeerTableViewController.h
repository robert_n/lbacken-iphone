//
//  BeerTableViewController.h
//  BeerCrate
//
//  Created by Robert Nordström on 2013-03-23.
//  Copyright (c) 2013 Robert N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeerTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *dataSource;

@end
