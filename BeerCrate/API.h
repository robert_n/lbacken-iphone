//
//  API.h
//  BeerCrate
//
//  Created by Robert Nordström on 2013-03-24.
//  Copyright (c) 2013 Robert N. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^JSONResponseBlock)(NSDictionary *json);

@interface API : NSObject

+ (API *)sharedInstance;

- (void)request:(JSONResponseBlock)completion;

+ (NSArray *)parseJSON:(NSDictionary *)json intoObjectOfType:(Class)objectClass;

@end
