//
//  AppDelegate.h
//  BeerCrate
//
//  Created by Robert Nordström on 2013-03-23.
//  Copyright (c) 2013 Robert N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
